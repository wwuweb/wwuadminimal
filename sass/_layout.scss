/**
 * @file
 * layout.scss
 *
 * This file contains CSS rules pertaining to LAYOUT and POSITIONING, including:
 *
 * border-width, left, line-height, padding, position, top, right, bottom, margin, height, width
 */

@media all {

  /* NAVBAR
   ***************************************************************************/
  #navbar-administration {
    .navbar-icon, 
    .navbar-menu-item {
      cursor: pointer;
    }

    .menu .menu {
      .navbar-icon.navbar-handle:before,
      .navbar-icon.navbar-handle.open:before {
        background-size: 50% auto;
      }
    }
  }

  /* EXPOSED FILTERS
   ***************************************************************************/
  .exposed-filters .additional-filters {
    line-height: 26px;
    padding: 0.5em 0;
  }

  .exposed-filters .form-select {
    margin-top: 0.5em;
    margin-bottom: 0.5em;
  }

  
  /* PAGE TABS (view, edit, etc.)
   ***************************************************************************/
  ul.primary {
    height: auto;
  }

  /* TABLES
   ***************************************************************************/
  table th.active a {
    position: relative;
  }

  table th.active img {
    position: absolute;
    right: 0;
  }

  body.adminimal-theme ul.secondary {

    li a,
    li a:hover,
    li.active a,
    li.active a.active {
      padding: 5px 10px;
    }
  }

  /* FORM ELEMENTS
   ***************************************************************************/
  #field-display-overview {
    
    input.field-formatter-settings-edit {
      padding: 6px;
    }
  }

  .container-inline div {
    display: inline-block;
  }

  /* VIEWS
   ***************************************************************************/
  .views-displays {

    .secondary .action-list {
      top: 100%;
    }

    .secondary .open > a {
      border: none;
    }
  }

  /* VIEWS ADMIN
   ***************************************************************************/
  ul#views-display-menu-tabs {
    width: auto;
    margin-right: 18em;

    li {
      margin-top: 5px;
      margin-bottom: 5px;
    }
  }

  .views-display-top .ctools-button-processed {
    margin-top: 5px;
  }

  /* PANELS
   ***************************************************************************/
  #page-manager-edit {
    .content-header .content-title {
      font-size: 150%;
      padding: 1em;
    }
  }

  /* VERTICAL TABS
   ***************************************************************************/
  div.vertical-tabs ul li.selected {
    a, a:hover, a:focus, a:active {
      margin-left: 0;
    }
  }

  /* MEDIA BROWSER INSERT DIALOG
   ***************************************************************************/
  #media-browser-tabset { 
    #branding h1 {
      height: auto;
    }

    ul.tabs.primary li {
      margin-right: 0.5em;
    }

    ul.tabs.primary li a:link {
      line-height: unset;
    }

    .ui-tabs-selected {
      padding-bottom: 0;
    }
  }

}

@media all and (min-width: 480px) {

  /* VERTICAL TABS
   ***************************************************************************/
  div.vertical-tabs ul li.selected {
    a, a:hover, a:focus, a:active {
      margin-left: -1px;
    }
  }

}
