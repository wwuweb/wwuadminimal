<?php

function wwuadminimal_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'user_profile_form') {
    $form['account']['current_pass']['#description'] = 'Enter your current password to change you <em>E-mail address</em>.';
    $form['account']['pass']['#access'] = user_access('administer users');
    $form['themekey_ui_themes']['#title'] = 'Theme configuration for my profile';
    $form['themekey_ui_themes']['#description'] = 'The departmental theme that will be displayed along with your profile (including the banner.)';
  }

}

